#ifndef SINTEFLIB_CALIBRATOR_H
#define SINTEFLIB_CALIBRATOR_H

#include <CDPSystem/Base/CDPComponent.h>
#include <Signal/CDPSignal.h>
#include <CDPParameter/CDPParameter.h>
#include <CDPAlarm/CDPAlarm.h>
#include <OSAPI/Process/OSAPIThread.h>
#include <OSAPI/Process/OSAPIEvent.h>
#include <CDPSystem/Base/CDPConnector.h>
#include  <iostream>
#include "Camera.h"

typedef std::vector< std::vector<double> > matrix;

namespace SintefLib {

class Calibrator : public CDPComponent, public OSAPIThread
{
public:
    Calibrator();
    ~Calibrator() override;

    virtual void Create(const char* fullName) override;
    virtual void CreateModel() override;
    virtual void Configure(const char* componentXML) override;
    void ProcessNull() override;
    virtual void Activate();
    virtual void Suspend();
    void SetPos(std::vector<double> pos, double vel, double acc);
    double CalculateDistance(double x0,double y0,double z0,double x1,double y1,double z1);
    std::pair<matrix, matrix> StorePoint();
    void Calibrate();
    void FindAndPoint();

    std::vector<double> home = {0.385, -0.085, 0.3};
    matrix transform;

    double normal_vel = 0.4;
    double normal_acc = 0.4;
    double approach_vel = 0.1;
    double approach_acc = 0.1;

protected:
    virtual void Main();
    void AccessObjectsUsingMemberAccessMutex();
    void AccessObjectsBySchedulingIntoComponentThread();

    using CDPComponent::requestedState;
    using CDPComponent::ts;
    using CDPComponent::fs;
    OSAPIEvent m_event;
    CDPParameter robotVel;
    CDPParameter robotAcc;
    CDPSignal<unsigned char> m_counter;
    CDPSignal<double> robotX;
    CDPSignal<double> robotY;
    CDPSignal<int> calibX;
    CDPSignal<int> calibY;
    CDPSignal<double> robotZ;
    CDPSignal<double> xRef;
    CDPSignal<double> yRef;
    CDPSignal<double> zRef;
    CDPSignal<double> fZ;
    CDPConnector SetRobPose;
};

} // namespace SintefLib

#endif
