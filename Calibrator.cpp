#include <OSAPI/Process/OSAPISemaphore.h>
#include <CDPSystem/Application/Application.h>

#include "Calibrator.h"
#include "URRobot.h"
#include "MatrixOperations.h"

using namespace SintefLib;

Calibrator::Calibrator()
{
}

Calibrator::~Calibrator()
{
}

void Calibrator::Create(const char* fullName)
{
    CDPComponent::Create(fullName);
    robotVel.Create("robotVel",this);
    robotAcc.Create("robotAcc",this);

    m_counter.Create("Counter",this); // Example signal counted by both main thread and state-machine
    robotX.Create("robotX",this);
    robotY.Create("robotY",this);
    robotZ.Create("robotZ",this);
    calibX.Create("calibX",this);
    calibY.Create("calibY",this);
    fZ.Create("fZ",this);

    xRef.Create("xRef",this);
    yRef.Create("yRef",this);
    zRef.Create("zRef",this);

    SetRobPose.Create("SetRobPose",this);

}

void Calibrator::CreateModel()
{
    CDPComponent::CreateModel();

    RegisterStateProcess("Null", (CDPCOMPONENT_STATEPROCESS)&Calibrator::ProcessNull, "Initial Null state");
}

void Calibrator::Configure(const char* componentXML)
{
    CDPComponent::Configure(componentXML);
}

void Calibrator::ProcessNull()
{
    m_event.Set(); // this triggers the main thread
    m_counter += 10;
}

void Calibrator::Activate()
{
    CDPComponent::Activate();
    // Start the Main() thread with name of the component and normal priority:
    //Start(CDPTHREAD_PRIORITY_NORMAL,ShortName());
    Application::RunInMainThread([&] { Main(); });
}

void Calibrator::Suspend()
{
    CDPComponent::Suspend();
    Stop();           // set Stop flag
    m_event.Set();    // set event so that Wait in Main() completes.
}

void Calibrator::SetPos(std::vector<double> pos, double vel, double acc)
{
    // Set signal values for target position
    xRef.SetDouble(pos[0]);
    yRef.SetDouble(pos[1]);
    zRef.SetDouble(pos[2]);

    robotVel.SetValue(vel);
    robotAcc.SetValue(acc);

    // Wait some seconds for the variables to be set
    CDPTimer timer;
    timer.Reset(1);
    timer.Start();
    while (!timer.TimedOut()){}
    // Move robot
    SetRobPose.SendMessage("SetPose");

    // Wait until robot is 0.5mm from target pose
    while(CalculateDistance(robotX.GetDouble(),
                            robotY.GetDouble(),
                            robotZ.GetDouble(),
                            pos[0], pos[1], pos[2])>0.0005) {}
}

double Calibrator::CalculateDistance(double x0, double y0, double z0,
                                     double x1, double y1, double z1)
{
    /* Calculates euclidean distance between target
     * and current robot position
     */
    double dist = sqrt(pow(x1-x0,2) + pow(y1-y0,2) + pow(z1-z0,2));
    return dist;
}

std::pair<matrix, matrix> Calibrator::StorePoint()
{
    /* Moves calibration to a position,
     * we store robot pose and calib object pose
     * detected by camera. Afterwards we
     * will use the information for calculating
     * a transformation matrix
     */

    Camera cam;
    MatrixOperations matOps;

    // Vectors that will store our point values
    matrix calibObjPoints;
    matrix robotPosPoints;

    // Poses where we want to move our calibration object
    matrix calibPoses = {
        {0.208,-0.866,-0.1},
        {0.4,-0.833,-0.1},
        {0.4,-0.581,-0.1}
       };

    std::vector<double> pos;
    std::vector<double> approach;
    std::vector<double> cam_res;

    cam.TuneCalibration();
    for (unsigned int i=0; i<calibPoses.size(); i++)
    {
        if (i==0)
        {
            pos = calibPoses[i];
            approach = pos;
            approach[2] += 0.05;
        }

        SetPos(approach, normal_vel, normal_acc);
        SetPos(pos, approach_vel, approach_acc);

        pos = calibPoses[i];
        approach = pos;
        approach[2] += 0.05;

        //SetPos(approach, 0.05, 0.05);
        SetPos(pos, normal_vel, normal_acc);
        SetPos(approach, approach_vel, approach_acc);

        robotPosPoints.push_back({robotX.GetDouble(), robotY.GetDouble()});

        SetPos(home, normal_vel, normal_acc);
        cam_res = cam.FindCalibObject();

        calibX = cam_res[0];
        calibY = cam_res[1];

        //CDPMessage("calibX: %f", cam_res[0]);
        //CDPMessage("calibY: %f", cam_res[1]);

        calibObjPoints.push_back(cam_res);

    }
    //matOps.PrintVector(calibObjPoints);
    //matOps.PrintVector(robotPosPoints);

    return std::make_pair(calibObjPoints, robotPosPoints);
}

void Calibrator::Calibrate()
{
    std::pair<matrix, matrix> calibPoints, preparedMatrices;
    MatrixOperations matOps;

    calibPoints = StorePoint();
    preparedMatrices = matOps.PrepareMatrices(calibPoints);

    matrix cal = std::get<0>(preparedMatrices);
    matrix rob = std::get<1>(preparedMatrices);

    matOps.PrintVector(cal);
    matOps.PrintVector(rob);

    transform = matOps.SolveLeastSquares(rob, cal);

    matOps.PrintVector(transform);
}

void Calibrator::FindAndPoint()
{
    /* Finds calibration object and moves robot to
     * point the object
     */

    Camera cam;
    MatrixOperations matOps;

    matrix objectPos, objectPos_t;
    matrix robotPos;

    std::vector<double> op;

    // Wait some seconds for letting the robot connection start
    CDPTimer timer;
    timer.Reset(3);
    timer.Start();
    while (!timer.TimedOut()){}

    SetPos(home, normal_vel, normal_acc);

    op = cam.FindCalibObject();
    calibX = op[0];
    calibY = op[1];

    // Append a one
    op.push_back(1);
    // row in a matrix
    objectPos.push_back(op);

    objectPos_t = matOps.Transpose(objectPos);

    //matOps.PrintVector(objectPos);
    //matOps.PrintVector(objectPos_t);
    //matOps.PrintVector(transform);

    robotPos = matOps.Dot(transform, objectPos_t);
    matOps.PrintVector(robotPos);

    std::vector<double> pos = {robotPos[0][0], robotPos[1][0], -0.1};
    std::vector<double> approach(pos);
    approach[2] += 0.05;

    //CDPMessage("%f, %f, %f\n", approach[0], approach[1], approach[2]);
    //CDPMessage("%f, %f, %f\n", pos[0], pos[1], pos[2]);

    SetPos(approach, normal_vel, normal_acc);
    SetPos(pos, approach_vel, approach_acc);
    SetPos(approach, approach_vel, approach_acc);
}


/*!
  brief Main thread function, runs asynchronously from state-machine.

  Note that it is not safe to access cdp signals, parameters, alarms etc. from a thread.
  The helper RunInComponentThread() and the component member access mutex ( GetMemberAccessMutex() )
  can be used to safely access cdp objects. As in the process functions, any timeconsuming processing
  performed while the member access mutex is locked may impact the scheduling of other components running
  at the same priority as this component.
*/
void Calibrator::Main()
{
    /* Define a number of poses and move calibration object to them,
     * then, read robot X and Y, move robot away, and detect X and Y for calib object
     * then, calculate transformation matrix by solving equation
     */

    transform = {{5.11987e-005, -0.00178584, 0.649898},
                 {-0.00179963, -3.93927e-005, -0.25665},
                 {0, 0, 1}};

    Calibrate();

    for (int i=0; i<3; i++)
    {
        FindAndPoint();
    }

    SetPos(home, normal_vel, normal_acc);

    //Application::RunInMainThread([&] { Main(); });

}

/*!
  brief Shows how to safely access cdp objects using the memberaccess mutex
*/
void Calibrator::AccessObjectsUsingMemberAccessMutex()
{
    OSAPIMutexLocker locker(GetMemberAccessMutex(),"Main thread member access"); // automatic unlock when object is destroyed
    m_counter += 20;  // It is now safe to access variables, but note that locking the mutex will block
    // the component statemachine from running for the duration of the lock.
}

/*!
  brief Executes the toggling of m_toggleSignal the next time the component is run, after state-transitions and state run:
*/
void Calibrator::AccessObjectsBySchedulingIntoComponentThread()
{
    RunInComponentThread([&] () { m_counter += 70; });
}
