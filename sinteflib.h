/**
SintefLib header file. Include this file in the project to use the library.
*/
#ifndef SINTEFLIB_SINTEFLIB_H
#define SINTEFLIB_SINTEFLIB_H

#include "SintefLibBuilder.h"

namespace SintefLib {

/** Instantiate the SintefLibBuilder for this object */
SintefLibBuilder gSintefLibBuilder("SintefLib", __TIMESTAMP__);

}

#endif // SINTEFLIB_SINTEFLIB_H
