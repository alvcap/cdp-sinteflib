#ifndef SINTEFLIB_ATIFT_H
#define SINTEFLIB_ATIFT_H

#include <CDPSystem/Base/CDPComponent.h>
#include <Signal/CDPSignal.h>
#include <CDPParameter/CDPParameter.h>
#include <CDPAlarm/CDPAlarm.h>
#include <OSAPI/Process/OSAPIThread.h>
#include <OSAPI/Process/OSAPIEvent.h>
#include <IO/Transport.h>

namespace SintefLib {

class ATIFT : public CDPComponent, public OSAPIThread
{
public:
    ATIFT();
    ~ATIFT() override;

    virtual void Create(const char* fullName) override;
    virtual void CreateModel() override;
    virtual void Configure(const char* componentXML) override;
    void ProcessNull() override;
    int MessageSaveBias(void* message);
    virtual void Activate();
    virtual void Suspend();
protected:
    void SendInitialCommand();
    void ReadResponse();
    virtual bool HandleXMLElement(XMLElementEx *pEx) override;
    virtual void Main();

    using CDPComponent::requestedState;
    using CDPComponent::ts;
    using CDPComponent::fs;
    CDPParameter FxBias;
    CDPParameter FyBias;
    CDPParameter FzBias;
    CDPParameter TxBias;
    CDPParameter TyBias;
    CDPParameter TzBias;
    CDPSignal<int> Fx;
    CDPSignal<int> Fy;
    CDPSignal<int> Fz;
    CDPSignal<int> Tx;
    CDPSignal<int> Ty;
    CDPSignal<int> Tz;
    CDP::IO::Transport* m_transport;
};

} // namespace SintefLib

#endif
