#include "MatrixOperations.h"

namespace SintefLib {

MatrixOperations::MatrixOperations()
{
}
void MatrixOperations::PrintVector(matrix vec)
{
    std::cout << "[" << std::endl;

    for (size_t i=0; i<vec.size(); i++)
    {
        std::cout << " [";
        for (size_t j=0; j<vec[i].size(); j++)
        {
            std::cout << vec[i][j];

            if (j!=vec[i].size()-1)
                std::cout << ", ";
        }
        std::cout << "]" << std::endl;
    }

    std::cout << "]" << std::endl;
}

matrix MatrixOperations::Transpose(matrix vec)
{
    int rows = vec.size();
    int cols = vec[0].size();
    matrix transposed(cols, std::vector<double> (rows));

    for (size_t i=0; i<vec.size(); i++)
    {
        for (size_t j=0; j<vec[i].size(); j++)
        {
            transposed[j][i] = vec[i][j];
        }
    }

    return transposed;
}

std::pair<matrix, matrix> MatrixOperations::PrepareMatrices(std::pair<matrix, matrix> inputMatrices)
{
    /*
     * Gets a pair of matrices having this structure
     * | x1 y1    |
     * | x2 y2 ...|
     *
     * And returns the matrices as
     *
     * | x1 x2     |
     * | y1 y2 ... |
     * |  1  1     |
     *
     */

    matrix m0 = std::get<0>(inputMatrices);
    matrix m1 = std::get<1>(inputMatrices);

    matrix tr_m0 = Transpose(m0);
    matrix tr_m1 = Transpose(m1);

    // Add a row of ones
    std::vector<double> ones;

    for(size_t i=0; i<tr_m0[0].size(); i++)
    {
        ones.push_back(1.0);
    }

    tr_m0.push_back(ones);
    tr_m1.push_back(ones);

    return std::make_pair(tr_m0, tr_m1);
}

double MatrixOperations::Determinant(matrix mat, int dimension, bool init)
{
    /* Adaptation of this example:
     * https://www.sanfoundry.com/cpp-program-compute-determinant-matrix/
     *
     * init variable specifies if the calculation is initialised
     * there is some recursion inside this function that requires
     * to do so
     */

    if (init)
        det_aux = 0;

    matrix submat(10, std::vector<double>(10));

    if (dimension == 2)
        return ((mat[0][0] * mat[1][1]) - (mat[1][0] * mat[0][1]));
    else
    {
        for (int c=0; c<dimension; c++)
        {
            int subi = 0; //submatrix's i value
            for (int i=1; i<dimension; i++)
            {
                int subj = 0;
                for (int j=0; j<dimension; j++)
                {
                    if (j == c)
                        continue;
                    submat[subi][subj] = mat[i][j];
                    subj++;
                }
                subi++;
            }
            det_aux = det_aux + (pow(-1, c) * mat[0][c] * Determinant(submat, dimension-1, false));
        }
    }

    return det_aux;
}

matrix MatrixOperations::Inverse(matrix mat)
{
    /*
     * http://mathworld.wolfram.com/MatrixInverse.html
     */

    matrix inverted(mat.size(), std::vector<double>(mat[0].size()));
    matrix submatrix;
    std::vector<double> subrow;

    int dimension = mat.size();
    double mat_determinant = Determinant(mat, mat.size(), true);
    matrix transposed = Transpose(mat);

    for (int i=0; i<dimension; i++)
    {
        for (int j=0; j<dimension; j++)
        {
            submatrix.clear();

            for (int ii=0; ii<dimension; ii++)
            {
                subrow.clear();

                for (int jj=0; jj<dimension; jj++)
                {
                    if (!((i==ii) || (j==jj)))
                        subrow.push_back(transposed[ii][jj]);
                }

                if (!subrow.empty())
                    submatrix.push_back(subrow);
            }
            if (!submatrix.empty())
            {
                if ((i+j)%2==0)
                    inverted[i][j] = Determinant(submatrix, submatrix.size(), true) / mat_determinant;
                else
                    inverted[i][j] = -1 * Determinant(submatrix, submatrix.size(), true) / mat_determinant;
            }
        }
    }
    return inverted;
}

matrix MatrixOperations::Dot(matrix a, matrix b)
{
    /* Adapted from:
     * https://www.programiz.com/cpp-programming/examples/matrix-multiplication
     */

    int rows_a = a.size();
    int cols_a = a[0].size();
    int rows_b = b.size();
    int cols_b = b[0].size();

    matrix mult(rows_a, std::vector<double>(cols_b));
    // Initializing elements of matrix mult to 0.
    for(int i = 0; i < rows_a; ++i)
        for(int j = 0; j < cols_b; ++j)
        {
            mult[i][j]=0;
        }

    if (cols_a != rows_b)
    {
        std::cout << "DotProduct: Matrix sizes do not match\n";
        return mult;
    }

    // Multiplying matrix a and b and storing in array mult.
    for(int i = 0; i < rows_a; ++i)
        for(int j = 0; j < cols_b; ++j)
            for(int k = 0; k < cols_a; ++k)
            {
                mult[i][j] += a[i][k] * b[k][j];
            }

    return mult;
}

matrix MatrixOperations::SolveLeastSquares(matrix rob, matrix cam)
{
    /* Used for solving overdetermined systems
     * https://s-mat-pcs.oulu.fi/~mpa/matreng/eem5_5-1.htm
     *
     * rob = T*cam >> T = rob*cam_t*(cam*cam_t)^-1
     */

    matrix cam_tr = Transpose(cam);
    matrix cam_cam_tr = Dot(cam, cam_tr);

    matrix cam_cam_tr_inv = Inverse(cam_cam_tr);
    matrix mat = Dot(cam_tr, cam_cam_tr_inv);
    matrix solution = Dot(rob, mat);

    return solution;
}

} // namespace SintefLib

