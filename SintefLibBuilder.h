/**
SintefLibBuilder header file.
*/

#ifndef SINTEFLIB_SINTEFLIBBUILDER_H
#define SINTEFLIB_SINTEFLIBBUILDER_H

#include <CDPSystem/Application/CDPBuilder.h>

namespace SintefLib {

class SintefLibBuilder : public CDPBuilder
{
public:
    SintefLibBuilder(const char* libName,const char* timeStamp);
    CDPComponent* CreateNewComponent(const std::string& type) override;
    CDPBaseObject* CreateNewCDPOperator(const std::string& modelName,const std::string& type,const CDPPropertyBase* inputProperty) override;
};

}

#endif // SINTEFLIB_SINTEFLIBBUILDER_H
