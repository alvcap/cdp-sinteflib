CDPVERSION = 4.5
TYPE = library
PROJECTNAME = SintefLib

DEPS += \

HEADERS += \
    sinteflib.h \
    SintefLibBuilder.h \
    ATIFT.h \
    URRobot.h \
    Camera.h \
    Calibrator.h \
    MatrixOperations.h

SOURCES += \
    SintefLibBuilder.cpp \
    ATIFT.cpp \
    URRobot.cpp \
    Camera.cpp \
    Calibrator.cpp \
    MatrixOperations.cpp

DISTFILES += $$files(*.xml, true) \
    Templates/Models/SintefLib.ATIFT.xml \
    Templates/Models/SintefLib.URRobot.xml \
    Templates/Models/SintefLib.Camera.xml \
    Templates/Models/SintefLib.Calibrator.xml

load(cdp)
