#ifndef SINTEFLIB_URROBOT_H
#define SINTEFLIB_URROBOT_H

#include <CDPSystem/Base/CDPComponent.h>
#include <Signal/CDPSignal.h>
#include <CDPParameter/CDPParameter.h>
#include <CDPAlarm/CDPAlarm.h>
#include <OSAPI/Process/OSAPIThread.h>
#include <OSAPI/Process/OSAPIEvent.h>
#include <IO/Transport.h>
#include <OSAPI/Process/OSAPISemaphore.h>
#include <CDPSystem/Messenger/Messenger.h>
#include <sstream>

namespace SintefLib {

class URRobot : public CDPComponent, public OSAPIThread
{
public:
  URRobot();
  ~URRobot() override;

  virtual void Create(const char* fullName) override;
  virtual void CreateModel() override;
  virtual void Configure(const char* componentXML) override;
  void ProcessNull() override;
  int MessageSetPose(void*);
  void SetPos(double, double, double);
  virtual void Activate();
  virtual void Suspend();
protected:
  void WriteCommandsIfNeeded();
  void ReadSecondaryMonitor();
  virtual bool HandleXMLElement(XMLElementEx *pEx) override;
  virtual void Main();

  using CDPComponent::requestedState;
  using CDPComponent::ts;
  using CDPComponent::fs;
  OSAPIEvent m_event;
  CDPParameter velocity;
  CDPParameter acceleration;
  CDPSignal<double> x;
  CDPSignal<double> y;
  CDPSignal<double> z;
  CDPSignal<double> rx;
  CDPSignal<double> ry;
  CDPSignal<double> rz;
  CDPSignal<double> xRef;
  CDPSignal<double> yRef;
  CDPSignal<double> zRef;
  CDPSignal<double> rxRef;
  CDPSignal<double> ryRef;
  CDPSignal<double> rzRef;
  CDP::IO::Transport* m_transport;
  unsigned int m_size{0};
  unsigned char m_type{0};
  unsigned char m_data[10000]={0};

private:
  void AnalyzeHeader();
  double GetDoubleFromData(unsigned char **dataPtr);
};

} // namespace SintefLib

#endif
