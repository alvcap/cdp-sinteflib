#ifndef SINTEFLIB_CAMERA_H
#define SINTEFLIB_CAMERA_H

#include <CDPSystem/Base/CDPComponent.h>
#include <Signal/CDPSignal.h>
#include <CDPParameter/CDPParameter.h>
#include <CDPAlarm/CDPAlarm.h>
#include <OSAPI/Process/OSAPIThread.h>
#include <OSAPI/Process/OSAPIEvent.h>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

namespace SintefLib {

class Camera : public CDPComponent, public OSAPIThread
{
public:
  Camera();
  ~Camera() override;

  virtual void Create(const char* fullName) override;
  virtual void CreateModel() override;
  virtual void Configure(const char* componentXML) override;
  void ProcessNull() override;
  virtual void Activate();
  virtual void Suspend();

  cv::VideoCapture cam;
  void Connect(int num);
  void Disconnect();
  cv::Mat GetFrame();
  cv::Mat DrawCircles(cv::Mat, std::vector<cv::Vec3f>);
  std::vector<double> FindCalibObject();
  void TuneCalibration();

  int thres;
  int cir_param1;
  int cir_param2;
  int cir_minRadius;
  int cir_maxRadius;

  const char* windowName;

protected:
  virtual void Main();
  void AccessObjectsUsingMemberAccessMutex();
  void AccessObjectsBySchedulingIntoComponentThread();

  using CDPComponent::requestedState;
  using CDPComponent::ts;
  using CDPComponent::fs;
  OSAPIEvent m_event;
  CDPSignal<unsigned char> m_counter;
  CDPSignal<int> calibObjectX;
  CDPSignal<int> calibObjectY;

};

} // namespace SintefLib

#endif
