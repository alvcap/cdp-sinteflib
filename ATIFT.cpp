#include "ATIFT.h"
#include <OSAPI/Process/OSAPISemaphore.h>
using namespace SintefLib;

namespace
{
typedef struct response_struct
{
  uint32_t rdt_sequence;
  uint32_t ft_sequence;
  uint32_t status;
  int32_t Fx;
  int32_t Fy;
  int32_t Fz;
  int32_t Tx;
  int32_t Ty;
  int32_t Tz;
} RESPONSE;
}

ATIFT::ATIFT()
{
}

ATIFT::~ATIFT()
{
}

void ATIFT::Create(const char* fullName)
{
  CDPComponent::Create(fullName);
  FxBias.Create("FxBias",this);
  FyBias.Create("FyBias",this);
  FzBias.Create("FzBias",this);
  TxBias.Create("TxBias",this);
  TyBias.Create("TyBias",this);
  TzBias.Create("TzBias",this);
  Fx.Create("Fx",this);
  Fy.Create("Fy",this);
  Fz.Create("Fz",this);
  Tx.Create("Tx",this);
  Ty.Create("Ty",this);
  Tz.Create("Tz",this);

}

void ATIFT::CreateModel()
{
  CDPComponent::CreateModel();

  RegisterStateProcess("Null", (CDPCOMPONENT_STATEPROCESS)&ATIFT::ProcessNull, "Initial Null state");
  RegisterMessage(CM_TEXTCOMMAND,"SaveBias","",(CDPOBJECT_MESSAGEHANDLER)&ATIFT::MessageSaveBias);
}

void ATIFT::Configure(const char* componentXML)
{
  CDPComponent::Configure(componentXML);
}

void ATIFT::ProcessNull()
{
}



int ATIFT::MessageSaveBias(void* message)
{
  FxBias = Fx;
  FyBias = Fy;
  FzBias = Fz;

  TxBias = Tx;
  TyBias = Ty;
  TzBias = Tz;

  return 1;
}

void ATIFT::Activate()
{
  CDPComponent::Activate();
  // Start the Main() thread with name of the component and normal priority:
  Start(CDPTHREAD_PRIORITY_NORMAL,ShortName());
}

void ATIFT::Suspend()
{
  CDPComponent::Suspend();
  Stop();           // set Stop flag
}

void ATIFT::SendInitialCommand()
{
  unsigned char request[8];                     // Request data sent to Net F/T
  uint16_t* header = (uint16_t*)&request[0];
  header[0] = htons(0x1234);      // Standard header
  header[1] = htons(2);      // Standard header
  header[2] = 0;  // Cheating a bit here for impl. simplicity
  header[3] = 0;  // these two are actually one 32 bit int.
  if(DebugLevelForComponent(this,DEBUGLEVEL_NORMAL))
    CDPMessage("%s: Sending initial command.\n",CDPComponent::Name());

  /*unsigned int bytesWritten = */ m_transport->Write((const char*)request,8);
}

void ATIFT::ReadResponse()
{
  RESPONSE* resp;                  // Structured response received from Net F/T
  unsigned char response[sizeof(RESPONSE)];     // Raw response data received from Net F/T


  // Receive the response
  if( m_transport->Read((char*)response, sizeof(RESPONSE),0.002)==sizeof(RESPONSE))
  {
    if(DebugLevelForComponent(this,DEBUGLEVEL_EXTENDED))
      CDPMessage("%s: Read response ok.\n",CDPComponent::Name());
    resp = reinterpret_cast<RESPONSE*>(response);
    // todo update signals:
    OSAPIMutexLocker locker(GetMemberAccessMutex(),"ReadResponse");
    Fx = ntoh<int32_t>(resp->Fx) - FxBias;
    Fy = ntoh<int32_t>(resp->Fy) - FyBias;
    Fz = ntoh<int32_t>(resp->Fz) - FzBias;
    Tx = ntoh<int32_t>(resp->Tx) - TxBias;
    Ty = ntoh<int32_t>(resp->Ty) - TyBias;
    Tz = ntoh<int32_t>(resp->Tz) - TzBias;
  }
  else
    if(DebugLevelForComponent(this,DEBUGLEVEL_NORMAL))
      CDPMessage("%s: Error reading response.\n",CDPComponent::Name());
}

bool ATIFT::HandleXMLElement(XMLElementEx *pEx)
{
  if (pEx->GetName()=="Transport")
  {
    m_transport = CDP::IO::Transport::Create(pEx,this);
    if (m_transport==nullptr)
      Suspend();
    else
      m_transport->Configure(pEx,this);
    return true;
  }
  return CDPComponent::HandleXMLElement(pEx); // call base
}

/*!
  brief Main thread function, runs asynchronously from state-machine.

  Note that it is not safe to access cdp signals, parameters, alarms etc. from a thread.
  The helper RunInComponentThread() and the component member access mutex ( GetMemberAccessMutex() )
  can be used to safely access cdp objects. As in the process functions, any timeconsuming processing
  performed while the member access mutex is locked may impact the scheduling of other components running
  at the same priority as this component.
*/
void ATIFT::Main()
{
  while(!Stopped())
  {
    if(!Stopped())
    {
      if(!m_transport->IsOpen())
      {
        if(m_transport->Open(CDP::IO::Transport::OpenMode_Send))
        {
          if(DebugLevelForComponent(this,DEBUGLEVEL_NORMAL))
            CDPMessage("%s: Transport opened, sending initial command.\n",CDPComponent::Name());
          SendInitialCommand();
        }
        else
        {
          if(DebugLevelForComponent(this,DEBUGLEVEL_NORMAL))
            CDPMessage("%s: Transport failed to open?!\n",CDPComponent::Name());
        }
      }
      else
        ReadResponse();
    }
  }
  if(m_transport->IsOpen())
    m_transport->Close();
}
