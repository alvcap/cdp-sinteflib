#ifndef SINTEFLIB_MATRIXOPERATIONS_H
#define SINTEFLIB_MATRIXOPERATIONS_H

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <utility>
#include <vector>

typedef std::vector< std::vector<double> > matrix;

namespace SintefLib {

class MatrixOperations
{
public:
    MatrixOperations();
    void PrintVector(matrix);
    matrix Transpose(matrix);
    std::pair<matrix, matrix> PrepareMatrices(std::pair<matrix, matrix>);
    double Determinant(matrix, int, bool);
    matrix Inverse(matrix);
    matrix Dot(matrix, matrix);
    matrix SolveLeastSquares(matrix, matrix);

    double det_aux;     // Auxiliar variable

};

} // namespace SintefLib

#endif // SINTEFLIB_MATRIXOPERATIONS_H
