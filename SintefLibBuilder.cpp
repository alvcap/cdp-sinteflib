/**
SintefLibBuilder implementation.
*/

#include "ATIFT.h"
#include "Camera.h"
#include "URRobot.h"
#include "Calibrator.h"
#include "SintefLibBuilder.h"

using namespace SintefLib;

SintefLibBuilder::SintefLibBuilder(const char* libName, const char* timeStamp)
    : CDPBuilder(libName, timeStamp)
{
}

CDPComponent* SintefLibBuilder::CreateNewComponent(const std::string& type)
{
    if (type=="SintefLib.ATIFT")
        return new ATIFT;
    

    if (type=="SintefLib.URRobot")
        return new URRobot;
    
    if (type=="SintefLib.Camera")
        return new Camera;

    if (type=="SintefLib.Calibrator")
        return new Calibrator;
    
    return CDPBuilder::CreateNewComponent(type);
}

CDPBaseObject* SintefLibBuilder::CreateNewCDPOperator(const std::string& modelName, const std::string& type, const CDPPropertyBase* inputProperty)
{
    return CDPBuilder::CreateNewCDPOperator(modelName, type, inputProperty);
}
