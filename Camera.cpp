#include <OSAPI/Process/OSAPISemaphore.h>
#include <CDPSystem/Application/Application.h>

#include "Camera.h"

using namespace SintefLib;
using namespace cv;

Camera::Camera()
{
    thres = 85;
    cir_param1 = 70;
    cir_param2 = 30;
    cir_minRadius = 9;
    cir_maxRadius = 15;
}

Camera::~Camera()
{
}

void Camera::Create(const char* fullName)
{
  CDPComponent::Create(fullName);

  m_counter.Create("Counter",this); // Example signal counted by both main thread and state-machine

  calibObjectX.Create("calibObjectX",this);
  calibObjectY.Create("calibObjectY",this);
}

void Camera::CreateModel()
{
  CDPComponent::CreateModel();

  RegisterStateProcess("Null", (CDPCOMPONENT_STATEPROCESS)&Camera::ProcessNull, "Initial Null state");
}

void Camera::Configure(const char* componentXML)
{
  CDPComponent::Configure(componentXML);
}

void Camera::ProcessNull()
{
  m_event.Set(); // this triggers the main thread
  m_counter += 10;
}

void Camera::Activate()
{
    CDPComponent::Activate();
    Application::RunInMainThread([&] { Main(); });
}

void Camera::Suspend()
{
  CDPComponent::Suspend();
  Stop();           // set Stop flag
  m_event.Set();    // set event so that Wait in Main() completes.
}

cv::Mat Camera::GetFrame()
{
    cv::VideoCapture cam(1);

    cv::Mat frame;
    cam.read(frame);
    cam.read(frame);    // Read two frames. Reason: buffer in camera

    cam.release();

    return frame;
}

Mat Camera::DrawCircles(Mat img, std::vector<Vec3f> circles)
{
    // Draw the circles detected
    for( size_t i = 0; i < circles.size(); i++ )
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);
        // circle center
        circle(img, center, 3, Scalar(0,255,0), -1, 8, 0 );
        // circle outline
        circle(img, center, radius, Scalar(0,0,255), 3, 8, 0 );
    }

    return img;
}

void Camera::TuneCalibration()
{
    windowName = "tune calibration";
    namedWindow(windowName,0);

    createTrackbar("threshold", windowName, &thres, 255);
    createTrackbar("cir_param1", windowName, &cir_param1, 255);
    createTrackbar("cir_param2", windowName, &cir_param2, 255);
    createTrackbar("cir_minRadius", windowName, &cir_minRadius, 255);
    createTrackbar("cir_maxRadius", windowName, &cir_maxRadius, 255);

    int waitKeyResponse;    /// Used when in tuning mode
    Mat grayFrame, thresholdResult, resultImg, frame;

    while (true)
    {  
        frame = Camera::GetFrame();
        cvtColor(frame, grayFrame, COLOR_RGB2GRAY);

        threshold(grayFrame, thresholdResult, thres, 255.0, THRESH_BINARY);
        std::vector<Vec3f> circles;
        HoughCircles(thresholdResult, circles, CV_HOUGH_GRADIENT,
                     2, thresholdResult.rows/4, static_cast<double>(cir_param1),
                     static_cast<double>(cir_param2), cir_minRadius, cir_maxRadius );

        // Convert to RGB for drawing circles
        cvtColor(thresholdResult, resultImg, COLOR_GRAY2RGB);
        resultImg = DrawCircles(resultImg, circles);

        imshow(windowName, resultImg);
        waitKeyResponse = waitKey(30);

        /// Quit when 'q' is pressed
        if (waitKeyResponse == 113)
        {
            break;
        }
    }
    destroyWindow("tune calibration");
}


std::vector<double> Camera::FindCalibObject()
{
    windowName = "find calib object";
    namedWindow(windowName,0);

    int waitKeyResponse;
    std::vector<double> result = {0, 0};
    Mat grayFrame, thresholdResult;

    Mat frame = Camera::GetFrame();
    cvtColor(frame, grayFrame, COLOR_RGB2GRAY);

    threshold(grayFrame, thresholdResult, thres, 255.0, THRESH_BINARY);
    std::vector<Vec3f> circles;
    HoughCircles(thresholdResult, circles, CV_HOUGH_GRADIENT,
                 2, thresholdResult.rows/4, static_cast<double>(cir_param1),
                 static_cast<double>(cir_param2), cir_minRadius, cir_maxRadius );

    DrawCircles(frame, circles);

    imshow(windowName, frame);
    waitKeyResponse = waitKey(30);

    if (circles.size() != 0)
    {
        result = {circles[0][0], circles[0][1]};
        calibObjectX = result[0];
        calibObjectY = result[1];

        //CDPMessage("calibX: %f", calibObjectX.GetDouble());
        //CDPMessage("calibY: %f", calibObjectY.GetDouble());
    }

    //destroyWindow("find calib object");
    return result;
}

void Camera::Main()
{
    //if (tuneCalib)
    //{
    //    Camera::TuneCalibration();
    //}

    //windowName = "Full Control";

    //Camera::FindCalibObject();

    //Application::RunInMainThread([&] { Main(); });
}

/*!
  brief Shows how to safely access cdp objects using the memberaccess mutex
*/
void Camera::AccessObjectsUsingMemberAccessMutex()
{
  OSAPIMutexLocker locker(GetMemberAccessMutex(),"Main thread member access"); // automatic unlock when object is destroyed
  m_counter += 20;  // It is now safe to access variables, but note that locking the mutex will block
  // the component statemachine from running for the duration of the lock.
}

/*!
  brief Executes the toggling of m_toggleSignal the next time the component is run, after state-transitions and state run:
*/
void Camera::AccessObjectsBySchedulingIntoComponentThread()
{
  RunInComponentThread([&] () { m_counter += 70; });
}
