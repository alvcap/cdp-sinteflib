#include "URRobot.h"

using namespace SintefLib;

URRobot::URRobot()
{
}

URRobot::~URRobot()
{
}

void URRobot::Create(const char* fullName)
{
  CDPComponent::Create(fullName);
  velocity.Create("velocity",this);
  acceleration.Create("acceleration",this);

  x.Create("x",this);
  y.Create("y",this);
  z.Create("z",this);
  rx.Create("rx",this);
  ry.Create("ry",this);
  rz.Create("rz",this);
  xRef.Create("xRef",this);
  yRef.Create("yRef",this);
  zRef.Create("zRef",this);
  rxRef.Create("rxRef",this);
  ryRef.Create("ryRef",this);
  rzRef.Create("rzRef",this);

}

void URRobot::CreateModel()
{
  CDPComponent::CreateModel();

  RegisterStateProcess("Null", (CDPCOMPONENT_STATEPROCESS)&URRobot::ProcessNull, "Initial Null state");
  RegisterMessage(CM_TEXTCOMMAND,"SetPose","Updates the robot with a new pose",(CDPOBJECT_MESSAGEHANDLER)&URRobot::MessageSetPose);
}

void URRobot::Configure(const char* componentXML)
{
  CDPComponent::Configure(componentXML);
}

void URRobot::ProcessNull()
{
}



int URRobot::MessageSetPose(void* /*message*/)
{
  m_event.Set(); // this triggers the sending of a new pose
  return 1;
}

void URRobot::Activate()
{
  CDPComponent::Activate();
  // Start the Main() thread with name of the component and normal priority:
  Start(CDPTHREAD_PRIORITY_NORMAL,ShortName());
}

void URRobot::Suspend()
{
  CDPComponent::Suspend();
  Stop();           // set Stop flag
  m_event.Set();    // set event so that Wait in Main() completes.
}

void URRobot::WriteCommandsIfNeeded()
{
  //CDPMessage("%s", m_event.IsSignalled() ? "true": "false");
  if(m_event.IsSignalled())
  {
    m_event.Reset();
    std::ostringstream prog;
    prog << "movel(p["  << xRef << ", "
                        << yRef << ", "
                        << zRef << ", "
                        << rx << ", "
                        << ry << ", "
                        << rz << "], "
                        << "a=" << acceleration << ", "
                        << "v=" << velocity << ", "
                        << "r=0)\n";
    CDPMessage(prog.str().c_str());
    if(m_transport->Write(prog.str().c_str(),prog.str().length())<prog.str().length())
      if(DebugLevelForComponent(this,DEBUGLEVEL_NORMAL))
        CDPMessage("%s: Error writing '%s' to robot.\n",CDPComponent::Name(),prog.str().c_str());

  }
}

void URRobot::AnalyzeHeader()
{
  /// First four bytes: message_size (int value)
  m_size =  m_data[0]<<24 |
            m_data[1]<<16 |
            m_data[2]<<8 |
            m_data[3];

  m_type =  m_data[4];

  if(DebugLevelForComponent(this,DEBUGLEVEL_NORMAL))
    CDPMessage("%s: Header size: %u, Type: %u\n",CDPComponent::Name(),m_size,m_type);
}

double URRobot::GetDoubleFromData(unsigned char** dataPtr)
{
  double val;
  FromBigendian(*dataPtr,val);
  (*dataPtr)+=8;
  return val;
}

void URRobot::ReadSecondaryMonitor()
{
  /// Get packages until we reach ROBOT_STATE message
  m_type = 0;
  while(!Stopped() && m_transport->IsOpen())
  {
    if(m_transport->Read((char*)m_data,5,0.01)==5)
    {
      AnalyzeHeader();
      m_transport->Read((char*)m_data+5,m_size-5);
      if(m_type==16)
        break;
    }
  }

  /// message_type=16: ROBOT_STATE message
  if(m_type==16 && !Stopped())
  {
    /// This works for PolyScope<3.5
    ///memcpy(m_data,m_data+302,101);

    /// PolyScope==3.5
    memcpy(m_data,m_data+303,101);
    AnalyzeHeader();

    if (m_type==4)    /// CARTESIAN DATA
    {
      unsigned char* dataPtr = m_data+5;
      x = GetDoubleFromData(&dataPtr);
      y = GetDoubleFromData(&dataPtr);
      z = GetDoubleFromData(&dataPtr);
      rx = GetDoubleFromData(&dataPtr);
      ry = GetDoubleFromData(&dataPtr);
      rz = GetDoubleFromData(&dataPtr);
    }
  }
}

bool URRobot::HandleXMLElement(XMLElementEx *pEx)
{
  if (pEx->GetName()=="Transport")
  {
    m_transport = CDP::IO::Transport::Create(pEx,this);
    if (m_transport==nullptr)
      Suspend();
    else
      m_transport->Configure(pEx,this);
    return true;
  }
  return CDPComponent::HandleXMLElement(pEx); // call base
}

/*!
  brief Main thread function, runs asynchronously from state-machine.

  Note that it is not safe to access cdp signals, parameters, alarms etc. from a thread.
  The helper RunInComponentThread() and the component member access mutex ( GetMemberAccessMutex() )
  can be used to safely access cdp objects. As in the process functions, any timeconsuming processing
  performed while the member access mutex is locked may impact the scheduling of other components running
  at the same priority as this component.
*/
void URRobot::Main()
{
  while(!Stopped())
  {
    if(!Stopped())
    {
      if(!m_transport->IsOpen())
      {
        if(m_transport->Open(CDP::IO::Transport::OpenMode_Send))
          CDPMessage("%s: Transport opened.\n",CDPComponent::Name());
        else
        {
          if(DebugLevelForComponent(this,DEBUGLEVEL_NORMAL))
            CDPMessage("%s: Transport failed to open?!\n",CDPComponent::Name());
        }
      }
      else
      {
        WriteCommandsIfNeeded();
        ReadSecondaryMonitor();
      }
    }
  }
  if(m_transport->IsOpen())
    m_transport->Close();
}
